import { Injectable } from '@angular/core';
import { Article } from '../models/articles.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  articles: Article[] = [];

  private url = 'https://jsonplaceholder.typicode.com/posts';

  constructor(public http: HttpClient) { }

  getArticles(): Observable<Article[]> {
    return this.http.get<Article[]>(this.url);
  }

  getArticleById(id: string): Observable<Article> {
    return this.http.get<Article>(this.url + '/' + id);
  }
  
}
