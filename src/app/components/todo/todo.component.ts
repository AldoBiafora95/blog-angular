import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/models/todo.model'; 
import { TodoService } from 'src/app/service/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  loading: boolean = false;
  todo: Todo[] = [];

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    this.getTodos();
  }

  getTodos(): void {
    this.loading = true;
    this.todoService.getTodos().subscribe(res => {
      this.todo = res;
      this.loading = false;
    });
  }


}
