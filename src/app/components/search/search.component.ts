import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/articles.model';
import { User } from 'src/app/models/users.model';
import { ArticleService } from 'src/app/service/article.service';
import { UsersService } from 'src/app/service/users.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  article!: Article;
  users: User[] = [];
  loading: boolean = false;
  
  constructor(private userService: UsersService, private articleService: ArticleService ,) { }

  ngOnInit(): void {
  }

  getArticleById(id: string): void {
    this.loading = true;
    this.articleService.getArticleById(id).subscribe(res => {
      this.article = res;
      this.loading = false;
    });
  }

  
  

}
